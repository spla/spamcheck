# Mastodon's spamcheck

Check and store data from last seven days sign-ups of your Mastodon server.

### Dependencies

-   **Python 3**
-   Postgresql server
-   [Mastodon](https://joinmastodon.org) server admin

### Usage:

Within Python Virtual Environment:

1. Run `pip install -r requirements.txt` to install needed Python libraries.

2. Run `python torips.py` to write Tor exit nodes IPs to new created database. It get the torbulkexitlist from [here](https://check.torproject.org/exit-addresses)  

3. Run `python spamcheck.py` for the first time to configure it. 

4. Use your favourite scheduling method to set `python spamcheck.py` and `python torips.py` to run regularly.  



